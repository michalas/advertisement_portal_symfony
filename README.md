Installation
--
```
$ git clone https://bitbucket.org/michal_alfalt/advertisement_portal_symfony.git
$ cd advertisement_portal_symfony/
$ composer install
$ php bin/console doctrine:database:create
$ php bin/console doctrine:schema:update --force
```

- - - - - - -  
###### © 2017 [Michal](https://bitbucket.org/michal_alfalt)


Training assignment for a PHP developer
--

Create a small web application that displays advertisements posted by users on its home page. It should somewhat resemble websites such as craigslist.org or skelbiu.lt, but have much less features.

**Requirements for the Application**

1. Both registered and unregistered users should see a list of all posted advertisements on the home page
2. Each advertisement in the list should display a posting date, title, detailed description and username of the poster
3. New users should be able to register by clicking "New User Registration" link on the homepage
4. Existing users should be able to log in
5. Logged in users should be able to post new advertisements. Posted advertisements should immediately appear on the home page
6. Logged in users should be able to see their own advertisements list
7. Logged in users should be able to log out
8. Deployment instructions

**Requirements**

* Framework - Symfony
* Frontend - Bootstrap
* Database - MySQL
* Version control - GIT
* Deployment - project must be placed on the internet and publicly available (free hosting available at heroku.com)


<?php
/**
 * Created by PhpStorm.
 * User: michal
 * Date: 06/11/17
 * Time: 02:31
 */


namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;


/**
 * Class AddNewAdvertisementForm
 *
 * Class is fully based on Symfony Form Component
 * Builds, validates and submits form at /subscription page.
 *
 * @package AppBundle\Form
 */
class AddNewAdvertisementForm extends AbstractType {

	/**
	 * Form building (from Symfony Examples)
	 *
	 * @param FormBuilderInterface $builder
	 * @param array $options
	 */
	public function buildForm( FormBuilderInterface $builder, array $options ) {
		$builder
			->add( 'Title', TextType::class, array(
				'label' => false,
				'attr'  => [
					'placeholder' => 'Title of your ad ...',
					'class'       => 'form-control',
				],
			) )
			->add( 'Description', TextareaType::class, array(
				'label' => false,
				'attr'  => [
					'placeholder' => 'Your ad description...',
					'class'       => 'form-control',
					'rows' => '7',
				],
			) );
	}
}
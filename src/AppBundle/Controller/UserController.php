<?php
/**
 * Created by PhpStorm.
 * User: michal
 * Date: 06/11/17
 * Time: 05:09
 */


namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class UserController extends Controller {


	/**
	 * Show currently logged users profile page
	 * Page contains such info as: ads list, ads count, last login date,
	 *
	 * @Route("/profile", name="profile")
	 *
	 * @return \Symfony\Component\HttpFoundation\Response
	 */
	public function viewProfile() {

		//check if user is authorized
		if ( ! $this->get( 'security.authorization_checker' )->isGranted( 'ROLE_USER' ) ) {
			throw $this->createAccessDeniedException();
		}

		//get a currently logged user ID from the Security Token Storage data.
		$user_id = $this->getUser()->getId();

		//get all rest data with Doctrine for currently logged user
		$user_ads = $this->getDoctrine()->getRepository( 'AppBundle:Advertisement' )->findBy( array( 'user_id' => $user_id ) );

		//send to Twig
		return $this->render( 'for_logged_users/profile.html.twig', [
			'user_ads' => $user_ads
		] );
	}

}



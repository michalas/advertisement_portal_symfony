<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Advertisement;
use AppBundle\Form\AddNewAdvertisementForm;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class AdvertisementController extends Controller {


	/**
	 * Method for adding new advertisements.
	 *
	 * @Route("/add-new-advertisement", name="add-new-advertisement")
	 *
	 * @param Request $request
	 *
	 * @return \Symfony\Component\HttpFoundation\Response
	 */
	public function addAdvertisement( Request $request ) {

		//check if user is authorized
		if ( ! $this->get( 'security.authorization_checker' )->isGranted( 'ROLE_USER' ) ) {
			throw $this->createAccessDeniedException();
		}

		//get a currently logged user ID from the Security Token Storage data.
		$user_id = $this->getUser()->getId();

		//create new advertisement object
		$new_ad = new Advertisement();

		//creating form from AddNewAdvertisementForm class
		$form = $this->createForm( AddNewAdvertisementForm::class, $new_ad );

		//handling form request
		$form->handleRequest( $request );

		//checking if form was submited and was valid
		if ( $form->isSubmitted() && $form->isValid() ) {


			//setting current time
			$new_ad->setDatetime( new \DateTime( 'now' ) );

			//set post author's id
			$new_ad->setUserId( $user_id );

			// you can fetch the EntityManager via $this->getDoctrine()
			// or you can add an argument to your action: createAction(EntityManagerInterface $em)
			$em = $this->getDoctrine()->getManager();

			// tells Doctrine you want to (eventually) save the Product (no queries yet)
			$em->persist( $new_ad );

			// actually executes the queries (i.e. the INSERT query)
			$em->flush();

			//add notification
			$this->addFlash(
				'notice',
				'New Advertisement was added successfully!'
			);

		}

		//send to twig and render advertisement adding form
		return $this->render( 'for_logged_users/add_advertisement.twig', [
			'form' => $form->createView()
		] );
	}




}

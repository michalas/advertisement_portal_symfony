<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Advertisement;
use AppBundle\Entity\User;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class HomepageController extends Controller {

	/**
	 * Homepage controller's indexAction() method displays all ads.
	 * Ads publish date and authors usernames are also displayed.
	 *
	 * :( i know it could be done nicely with MANYTOONE
	 * Doctrine stuff, but i am to soft for it ):
	 *
	 * @Route("/", name="homepage")
	 */
	public function indexAction() {

		//get all ads data (without user data)
		$all_ads_data = $this->getDoctrine()->getRepository( Advertisement::class )->findAll();

		//set array for later use - filling with ads author username
		$all_ads_full_data = array();

		//looping through all ads
		foreach ( $all_ads_data as $item ) {

			//get current ad author data
			$ad_author_data = $this->getDoctrine()->getRepository( User::class )->find( $item->getUserId() );

			//set current ad author username
			$item->username = $ad_author_data->getUsername();

			//generate new array with all needed data
			array_push( $all_ads_full_data, $item );

		}

		//send to Twig
		return $this->render( 'public/index.html.twig', [
			'ads_data' => $all_ads_full_data
		] );
	}


}

<?php
/**
 * Created by PhpStorm.
 * User: michal
 * Date: 03/11/17
 * Time: 12:22
 */
namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="advertisement")
 */
class Advertisement
{
	/**
	 * @ORM\Column(type="integer")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	private $id;

	/**
	 * @ORM\Column(type="datetime")
	 */
	private $datetime;

	/**
	 * @ORM\Column(type="string", length=200)
	 */
	private $title;


	/**
	 * @ORM\Column(type="text")
	 */
	private $description;


	/**
	 * @ORM\Column(name="user_id", type="integer")
	 */
	private $user_id;



	/**
	 * @return mixed
	 */
	public function getId() {
		return $this->id;
	}

	/**
	 * @return mixed
	 */
	public function getDatetime() {
		return $this->datetime;
	}

	/**
	 * @param mixed $datetime
	 */
	public function setDatetime( $datetime ) {
		$this->datetime = $datetime;
	}

	/**
	 * @return mixed
	 */
	public function getTitle() {
		return $this->title;
	}

	/**
	 * @param mixed $title
	 */
	public function setTitle( $title ) {
		$this->title = $title;
	}

	/**
	 * @return mixed
	 */
	public function getDescription() {
		return $this->description;
	}

	/**
	 * @param mixed $description
	 */
	public function setDescription( $description ) {
		$this->description = $description;
	}

	/**
	 * @return mixed
	 */
	public function getUserId() {
		return $this->user_id;
	}

	/**
	 * @param mixed $user_id
	 */
	public function setUserId( $user_id ) {
		$this->user_id = $user_id;
	}

//	/**
//	 * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User")
//	 * @ORM\JoinColumn(name="user_id", referencedColumnName="id", nullable=false)
//	 */
//	private $user;
//
//	/**
//	 * @return User
//	 */
//	public function getUser() {
//		return $this->user;
//	}


}

